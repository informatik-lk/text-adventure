package scenes;

import templates.*;
import templates.Object;
import templates.Character;
import characters.*;
import objects.*;

import java.util.ArrayList;
import java.util.Objects;

public class Outside extends Scene {
    public Outside() {
        this.name = "Outside";
        this.location = "Outside the Cheese Factory";

        this.characters = new ArrayList<Character>() {{
            add(new Boss("What are you doing here? Go and find the golden cheese!"));
            add(new Policeman("The golden cheese... Yes, I've heard of that. Unfortunately, I have no clue where it might be... Try asking around and do my job for me!"));
            add(new Peasant("I'm just a peasant. I don't know anything about the golden cheese. I'm just looking around..."));
            add(new CheeseThief("You want to know about the golden cheese? I'll tell you, but only if you give me some cheese first..."));
        }};

        this.objects = new ArrayList<Object>();

        this.ways = new ArrayList<Scene>();
    }

    @Override
    public void onItemDropped(Object object) {
        if (object instanceof Cheese) {
            this.objects.remove(object);
            this.characters.remove(3);
            this.characters.add(new CheeseThief("Thanks for the cheese! I've heard that my arch-nemesis, the Cheese Wizard, has the golden cheese. He's in the Cheese Castle, which is hidden somewhere in the Forest. Good luck!"));
        }
    }
}
