package scenes;

import templates.*;
import templates.Object;
import templates.Character;
import characters.*;
import objects.*;

import java.util.ArrayList;

public class Inside extends Scene {
    public Inside() {
        this.name = "Inside";
        this.location = "Cheese Factory";
        this.characters = new ArrayList<Character>() {{
            add(new Pig());
            add(new Boss("Bob, someone stole the golden cheese! As a world-renown cheese lover you are our prime suspect. You are fired! Only come back when you have found the golden cheese!"));
        }};

        this.objects = new ArrayList<Object>() {{
            add(new Cheese());
        }};

        this.ways = new ArrayList<Scene>() {{
            add(new Outside());
        }};
    }
}
