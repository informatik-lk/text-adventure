package characters;

import templates.Character;

public class Policeman extends Character {
    public Policeman(String dialogue) {
        this.type = "Human";
        this.name = "A Policeman";
        this.hp = 200;
        this.attack = 100;
        this.response = dialogue;
    }

    @Override
    public void onKill() {
        System.out.println("That was a bad idea! The policeman shoots you 35 times and you bleed to death.");
        System.exit(0);
    }
}
