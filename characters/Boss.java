package characters;

import templates.Character;

public class Boss extends Character {
    public Boss(String dialogue) {
        this.type = "Human";
        this.name = "Your Boss";
        this.hp = 10;
        this.attack = 5;
        this.response = dialogue;
    }

    @Override
    public void onKill() {
        System.out.println("What did you do that for? You're fired!");
        System.exit(0);
    }
}
