package characters;

import templates.Character;

public class Peasant extends Character {
    public Peasant(String dialogue) {
        this.type = "Human";
        this.name = "A Peasant";
        this.hp = 10;
        this.attack = 5;
        this.response = dialogue;
    }

    @Override
    public void onKill() {
        System.out.println("Wow, that's a bit harsh.");
    }
}
