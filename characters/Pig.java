package characters;

import templates.Character;

public class Pig extends Character {
    public Pig() {
        this.type = "Pig";
        this.name = "A Pig";
        this.hp = 5;
        this.attack = 1;
        this.response = "Oink!";
    }

    public void onKill() {
        System.out.println("Good call... Why was there a pig here anyways?");
    }
}
