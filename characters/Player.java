package characters;

import templates.Character;
import templates.Object;

import java.util.ArrayList;

public class Player extends Character {
    public ArrayList<Object> inventory = new ArrayList<Object>();
    public Player() {
        this.type = "Player";
        this.name = "Bob";
        this.hp = 100;
        this.attack = 10;
    }

    public void take(Object object) {
        this.inventory.add(object);
    }
}
