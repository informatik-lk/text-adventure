package characters;

import templates.Character;

public class CheeseThief extends Character {
    public CheeseThief(String dialogue) {
        this.type = "Human";
        this.name = "The Cheese Thief";
        this.hp = 20;
        this.attack = 5;
        this.response = dialogue;
    }

    @Override
    public void onKill() {
        System.out.println("Woah, calm down! Now I can't tell you where the golden cheese is... (Because I'm dead)");
    }
}
