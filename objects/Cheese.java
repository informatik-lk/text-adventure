package objects;

import templates.Object;

public class Cheese extends Object {
    public Cheese() {
        this.name = "Cheese";
        this.location = "On the table";
        this.quantity = 1;
    }

    public Cheese(int quantity) {
        this.name = "Cheese";
        this.location = "On the table";
        this.quantity = quantity;
    }

    public boolean use() {
        System.out.println("You eat the cheese. It's delicious.");
        return true;
    }
}
