package templates;

public class Character {
    public String type, name, response;
    public int hp, attack;

    public String getType() { return this.type; }
    public String getName() { return this.name; }
    public void onKill() { System.out.println(this.name + " is dead."); }
    public void kill() { this.hp = 0; this.onKill(); }
    public void talk() {
        System.out.println(this.name + ": " + this.response);
    }
}
