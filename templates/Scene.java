package templates;

import java.util.ArrayList;

public class Scene {
    public String name, location;
    public ArrayList<Character> characters;
    public ArrayList<Object> objects;
    public ArrayList<Scene> ways;

    public String getName() { return this.name; }

    public ArrayList<Character> getCharacters() { return this.characters; }

    public ArrayList<Object> getObjects() { return this.objects; }

    public String getLocation() { return this.location; }

    public ArrayList<Scene> getWays() { return this.ways; }

    public void onItemUse(Object object) {}

    public void onItemPickedUp(Object object) {}

    public void onItemDropped(Object object) {}
}
