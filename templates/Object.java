package templates;

public class Object {
    public String name, location;
    public int quantity;
    public String getName() { return this.name; }

    public String getLocation() { return this.location; }

    public int getQuantity() { return this.quantity; }

    public boolean use() {
        System.out.println("You can't use this object.");
        return false;
    }

    public boolean search() {
        System.out.println("You can't search this object.");
        return false;
    }
}
