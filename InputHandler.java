import java.util.Scanner;

public class InputHandler {
    private final Scanner scanner = new Scanner(System.in);
    public String[] handleInput() {
        String input = scanner.nextLine();
        return input.split("\\s+");
    }
}
