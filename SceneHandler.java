import characters.Player;
import com.sun.tools.jconsole.JConsoleContext;
import templates.Character;
import templates.Scene;
import templates.Object;

import java.io.IOException;
import java.util.Objects;

public class SceneHandler {
    Scene currentScene;
    Player player = new Player();
    public void load(Scene scene) {
        currentScene = scene;
        System.out.println("Loading " + scene.getName());
        System.out.println("You are at the " + scene.getLocation() + ".");
        System.out.println("You see the following characters:");
        for (int i = 0; i < scene.getCharacters().size(); i++) {
            System.out.printf("- %s\n", scene.getCharacters().get(i).getName());
        }

        for (int i = 0; i < scene.getObjects().size(); i++) {
            System.out.printf("%s, you see %s %s.\n", scene.getObjects().get(i).getLocation() , scene.getObjects().get(i).getQuantity(),scene.getObjects().get(i).getName());
        }

        String[] command = new InputHandler().handleInput();

        switch (command[0]) {
            case "go" -> {
                Scene way = getWay(command[1]);
                if (Objects.nonNull(way)) {
                    clearScreen();
                    this.load(way);
                } else {
                    clearScreen();
                    System.out.println("There is no way to " + command[1] + ".");
                    this.load(scene);
                }
            }
            case "take" -> {
                Object object = getObject(command[1]);
                if (Objects.nonNull(object)) {
                    scene.objects.remove(object);
                    player.inventory.add(object);
                    clearScreen();
                    System.out.println("You take the " + command[1] + ".");
                    scene.onItemPickedUp(object);
                    this.load(scene);
                } else {
                    clearScreen();
                    System.out.println("There is no " + command[1] + " here.");
                    this.load(scene);
                }
            }
            case "use" -> {
                Object object = getFromInventory(command[1]);
                if (Objects.nonNull(object)) {
                    clearScreen();
                    object.use();
                    player.inventory.remove(object);
                    scene.onItemUse(object);
                    this.load(scene);
                } else {
                    clearScreen();
                    System.out.println("There is no " + command[1] + " here.");
                    this.load(scene);
                }
            }
            case "talk" -> {
                Character character = getCharacter(command[1]);
                if (Objects.nonNull(character)) {
                    clearScreen();
                    character.talk();
                    this.load(scene);
                } else {
                    clearScreen();
                    System.out.println("There is no " + command[1] + " here.");
                    this.load(scene);
                }
            }
            case "kill" -> {
                Character character = getCharacter(command[1]);
                if (Objects.nonNull(character)) {
                    scene.characters.remove(character);
                    clearScreen();
                    character.kill();
                    this.load(scene);
                } else {
                    clearScreen();
                    System.out.println("There is no " + command[1] + " in your inventory.");
                    this.load(scene);
                }
            }
            case "drop" -> {
                Object object = getFromInventory(command[1]);
                if (Objects.nonNull(object)) {
                    object.location = "On the floor";
                    player.inventory.remove(object);
                    scene.objects.add(object);
                    clearScreen();
                    System.out.println("You drop the " + command[1] + ".");
                    scene.onItemDropped(object);
                    this.load(scene);
                } else {
                    clearScreen();
                    System.out.println("There is no " + command[1] + " in your inventory.");
                    this.load(scene);
                }
            }
        }
    }

    private Character getCharacter(String name) {
        for (int i = 0; i < currentScene.getCharacters().size(); i++) {
            if (currentScene.getCharacters().get(i).getName().contains(name))
                return currentScene.getCharacters().get(i);
        }
        return null;
    }

    private Object getObject(String name) {
        for (int i = 0; i < currentScene.getObjects().size(); i++) {
            if (currentScene.getObjects().get(i).getName().contains(name))
                return currentScene.getObjects().get(i);
        }
        return null;
    }

    private Object getFromInventory(String name) {
        for (int i = 0; i < player.inventory.size(); i++) {
            if (player.inventory.get(i).getName().contains(name))
                return player.inventory.get(i);
        }
        return null;
    }

    private Scene getWay(String name) {
        for (int i = 0; i < currentScene.getWays().size(); i++) {
            if (currentScene.getWays().get(i).getName().contains(name))
                return currentScene.getWays().get(i);
        }
        return null;
    }

    public static void clearScreen() {
        for (int i = 0; i < 50; ++i) System.out.println();
    }
}
