import scenes.*;

public class Main {
    public static void main(String[] args) {
        SceneHandler sceneHandler = new SceneHandler();
        sceneHandler.load(new Inside());
    }
}
